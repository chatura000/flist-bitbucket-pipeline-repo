package com.circle.vasstatus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VasStatusApplication {

	public static void main(String[] args) {
		SpringApplication.run(VasStatusApplication.class, args);
	}

}

package com.circle.vasstatus;

public class ExtraParams {
	private String details;

	private String fromNetwork;

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getFromNetwork() {
		return fromNetwork;
	}

	public void setFromNetwork(String fromNetwork) {
		this.fromNetwork = fromNetwork;
	}

}

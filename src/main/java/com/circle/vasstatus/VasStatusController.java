package com.circle.vasstatus;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@CrossOrigin
@RestController
public class VasStatusController {

	@Value("${remote.server.address}")
	private String serverAddress;

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	RestTemplate restTemplate;
	@RequestMapping(value = "/getVASStatus")
	public String getVasStatus(@RequestParam("inputPath") String inputFilepath, 
			//@RequestParam("testPath") String testPath,
			@RequestParam("outputPath") String outputFilepath, @RequestParam("secondsToSleep") String secondsToSleep) {
		inputFilepath = inputFilepath.replace("\\", "/");
		outputFilepath = outputFilepath.replace("\\", "/");
		//testPath = testPath.replace("\\", "/");

		BufferedReader reader;
		JSONObject jo = null;
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		JSONParser parser = new JSONParser();
//		JSONObject jooo = null;
//			
//				try {
//					Object obj = parser.parse(new FileReader(testPath));
//				    jooo = (JSONObject) obj;
//			//		System.out.println(jooo);
//					
//				} catch (FileNotFoundException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				} catch (IOException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				} catch (ParseException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
			
		
		try {
		reader = new BufferedReader(new FileReader(
				inputFilepath));
		FileWriter fileWriter = new FileWriter(outputFilepath);
		String line = reader.readLine();
		while (line != null) {
			
			jo = new JSONObject();
			JSONObject jo1 = new JSONObject();
			jo1.put("details", false);
			jo1.put("fromNetwork", true);
			jo.put("MSISDN", line.trim());
			jo.put("extraParams", jo1);
			
			HttpEntity<JSONObject> entity = new HttpEntity<JSONObject>(jo, headers);
			JSONObject results = restTemplate.exchange(serverAddress, HttpMethod.POST, entity, JSONObject.class).getBody();
					//jooo;
					
		
			//System.out.println(results);			
			
			JSONObject results1 =   (JSONObject) results.get("data");

			
			String keys =  results1.keySet().toString().replace('[', ' ').replace(']', ' ');
			
			String values = results1.values().toString().replace('[', ' ').replace(']', ' ');
			System.out.println(keys + "\n" + values + "\n");

			    fileWriter.write(keys + "\n" + values + "\n" + "\n");
			// read next line
			line = reader.readLine();
			int seconds = Integer.parseInt(secondsToSleep);
			try {
			    Thread.sleep(seconds * 1000);
			} catch (InterruptedException ie) {
			    Thread.currentThread().interrupt();
			}
		}
		reader.close();
		fileWriter.close();
	} catch (IOException e) {
		e.printStackTrace();
	}
		
		//System.out.println(filepath);
		
	
		
	//	System.out.println(jo);
		
	
		
		String s = "temp";
		return s;
		
	}
}

package com.circle.vasstatus;

public class VASEntity {
	private DataEntity data;

	private String errorCode;

	private String m1RefId;

	private String statusCode;

	private String status;

	public DataEntity getData() {
		return data;
	}

	public void setData(DataEntity data) {
		this.data = data;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getM1RefId() {
		return m1RefId;
	}

	public void setM1RefId(String m1RefId) {
		this.m1RefId = m1RefId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
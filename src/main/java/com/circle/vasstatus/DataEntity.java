package com.circle.vasstatus;

public class DataEntity {
	private String allIncomingCalls;

	private String allIncomingSMS;

	private String allOutgoingCalls;

	private String roaming;

	private String data;

	private String roamingIncomingSMS;

	private String effectivelySuspended;

	private String statusChangedDate;

	private String allOutgoingCallInternational;

	private String imsi;

	private String suspended;

	private String iccid;

	private String allOutgoingSMS;

	private String serviceStatus;

	private String imei;

	private String roamingIncomingCalls;

	private String callerId;

	private String msisdn;

	public String getAllIncomingCalls() {
		return allIncomingCalls;
	}

	public void setAllIncomingCalls(String allIncomingCalls) {
		this.allIncomingCalls = allIncomingCalls;
	}

	public String getAllIncomingSMS() {
		return allIncomingSMS;
	}

	public void setAllIncomingSMS(String allIncomingSMS) {
		this.allIncomingSMS = allIncomingSMS;
	}

	public String getAllOutgoingCalls() {
		return allOutgoingCalls;
	}

	public void setAllOutgoingCalls(String allOutgoingCalls) {
		this.allOutgoingCalls = allOutgoingCalls;
	}

	public String getRoaming() {
		return roaming;
	}

	public void setRoaming(String roaming) {
		this.roaming = roaming;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getRoamingIncomingSMS() {
		return roamingIncomingSMS;
	}

	public void setRoamingIncomingSMS(String roamingIncomingSMS) {
		this.roamingIncomingSMS = roamingIncomingSMS;
	}

	public String getEffectivelySuspended() {
		return effectivelySuspended;
	}

	public void setEffectivelySuspended(String effectivelySuspended) {
		this.effectivelySuspended = effectivelySuspended;
	}

	public String getStatusChangedDate() {
		return statusChangedDate;
	}

	public void setStatusChangedDate(String statusChangedDate) {
		this.statusChangedDate = statusChangedDate;
	}

	public String getAllOutgoingCallInternational() {
		return allOutgoingCallInternational;
	}

	public void setAllOutgoingCallInternational(String allOutgoingCallInternational) {
		this.allOutgoingCallInternational = allOutgoingCallInternational;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getSuspended() {
		return suspended;
	}

	public void setSuspended(String suspended) {
		this.suspended = suspended;
	}

	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getAllOutgoingSMS() {
		return allOutgoingSMS;
	}

	public void setAllOutgoingSMS(String allOutgoingSMS) {
		this.allOutgoingSMS = allOutgoingSMS;
	}

	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getRoamingIncomingCalls() {
		return roamingIncomingCalls;
	}

	public void setRoamingIncomingCalls(String roamingIncomingCalls) {
		this.roamingIncomingCalls = roamingIncomingCalls;
	}

	public String getCallerId() {
		return callerId;
	}

	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

}
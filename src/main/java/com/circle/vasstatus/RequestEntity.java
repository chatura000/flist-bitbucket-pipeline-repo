package com.circle.vasstatus;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestEntity {
	private String MSISDN;

	private ExtraParams extraParams;
	
    @JsonProperty("MSISDN")
	public String getMSISDN() {
		return MSISDN;
	}

	public void setMSISDN(String MSISDN) {
		this.MSISDN = MSISDN;
	}

	public ExtraParams getExtraParams() {
		return extraParams;
	}

	public void setExtraParams(ExtraParams extraParams) {
		this.extraParams = extraParams;
	}
}
